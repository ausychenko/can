#include <vector>
#include <assert.h>
#include <CAN_DEFINES.h>

using namespace CAN;

namespace CAN_A {
    typedef union {
        unsigned char data[8];
        BYTES bytes;
        struct {
            //D1
            uint16_t number_of_input_channel:1;
            uint16_t reserve_1:7;
            unsigned char:8;
            //D2
            uint16_t reserve_2:8;
            unsigned char:8;
            //D3
            uint16_t reserve_3:8;
            unsigned char:8;
            //D4
            uint16_t V_d:6;
            uint16_t reserve_4:2;
            unsigned char:8;
            //D5
            uint16_t increment:8;
            unsigned char:8;
            //D6
            uint16_t reserve_5:8;
            unsigned char:8;
            //D7
            uint16_t reserve_6:8;
            unsigned char:8;
            //D8
            uint16_t version_minor:4;
            uint16_t version_major:4;
            unsigned char:8;
        } bits/*__attribute__((aligned(8)))*/;
    } typePDO1;

    typePDO1 PDO1;
    typePDO1 *pPDO1 = &PDO1;

    typePDO1 PDO2;
    typePDO1 *pPDO2 = &PDO2;

    MAIL mail1 = {0x25A, 1, PDO1.data, sizeof(PDO1.data)};
    MAIL mail2 = {0x1DC, 2, PDO2.data, sizeof(PDO1.data)};

    void initialize();

    void setup();

    void setupMails();

    void enable(char data = 0);

    interrupt void execute();

    volatile unsigned long g_bErrFlag;  // A flag to indicate that some transmission error occurred.
}

void CAN_A::initialize() {
    CAN::initialize(CANA_BASE, 5, 4, 6, 6);
}

void CAN_A::enable(char data) {
    CAN::enable(CANA_BASE, data);
}

void CAN_A::setup() {
    CAN::setup(CANA_BASE, 500000);
}

void CAN_A::setupMails() {
    //
    // Initialize the message object that will be used for transmitting CAN
    // messages.
    //
    CANMsgSetTX(CANA_BASE, &mail1, MSG_OBJ_NO_FLAGS);
    //
    // Initialize the message object that will be used for receiving CAN
    // messages.
    //
    CANMsgSetRX(CANA_BASE, &mail2, MSG_OBJ_RX_INT_ENABLE);
    //
    // Setup the message object being used to receive messages
    //
}

interrupt void CAN_A::execute(void) {
    //
    // Read the CAN interrupt status to find the cause of the interrupt
    //
    unsigned long ulStatus = CANIntStatus(CANA_BASE, CAN_INT_STS_CAUSE);
    //
    // If the cause is a controller status interrupt, then get the status
//    printf("%lu\n", ulStatus);
    //
    if (ulStatus == CAN_INT_INT0ID_STATUS) {
//        printf("%lu\n", ulStatus);
        //
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors.  Error processing
        // is not done in this example for simplicity.  Refer to the
        // API documentation for details about the error status bits.
        // The act of reading this status will clear the interrupt.  If the
        // CAN peripheral is not connected to a CAN bus with other CAN devices
        // present, then errors will occur and will be indicated in the
        // controller status.
        //
        ulStatus = CANStatusGet(CANA_BASE, CAN_STS_CONTROL);
        //
        //Check to see if an error occurred.
        //
        if (((ulStatus & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 7) &&
            ((ulStatus & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 0)) {
            //
            // Set a flag to indicate some errors may have occurred.
            //
            g_bErrFlag = 1;
        }
    }
//
// Check if the cause is message object 1, which what we are using for
// sending messages.
//
    else if (ulStatus == mail1.box_number) {
        //
        // Getting to this point means that the TX interrupt occurred on
        // message object 1, and the message TX is complete.  Clear the
        // message object interrupt.
        //
        CANIntClear(CANA_BASE, mail1.box_number);
        //
        // Increment a counter to keep track of how many messages have been
        // sent.  In a real application this could be used to set flags to
        // indicate when a message is sent.
        //
//        g_ulTxMsgCount++;
        //
        // Since the message was sent, clear any error flags.
        //
        g_bErrFlag = 0;
    }
//
// Check if the cause is message object 2, which what we are using for
// receiving messages.
//
    else if (ulStatus == mail2.box_number) {
        //
        // Get the received message
        //
//        CANMessageGet(CANA_BASE, mail2.IDs.box, &mail2.CANMessage, true);
//        dataGet(mail2.data.bytes, mail2.bytes);

        CANMessageRX(CANA_BASE, &mail2);

        //
        // Getting to this point means that the TX interrupt occurred on
        // message object 1, and the message TX is complete.  Clear the
        // message object interrupt.
        //
        CANIntClear(CANA_BASE, mail2.box_number);
        //
        // Increment a counter to keep track of how many messages have been
        // sent.  In a real application this could be used to set flags to
        // indicate when a message is sent.
        //
//        g_ulRxMsgCount++;
        //
        // Since the message was sent, clear any error flags.
        //
        g_bErrFlag = 0;
//++++++++
        pPDO1->bits.version_minor = 15;
        pPDO1->bits.version_major = 15;
//++++++++
        CANMessageTX(CANA_BASE, &mail1);
    }

    CANGlobalIntClear(CANA_BASE, CAN_GLB_INT_CANINT0);
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}
