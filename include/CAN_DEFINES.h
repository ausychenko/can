#ifndef CAN_DEF_H
#define CAN_DEF_H

#include "F28x_Project.h"
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_can.h"
#include "driverlib/can.h"

namespace CAN {

    typedef struct {
        unsigned char D1;
        unsigned char D2;
        unsigned char D3;
        unsigned char D4;
        unsigned char D5;
        unsigned char D6;
        unsigned char D7;
        unsigned char D8;
    } BYTES;

    typedef struct {
        const uint32_t ID; // COB ID
        const uint32_t box_number; // box number
        unsigned char *pMsgData;
        uint16_t n; // message length
        tCANMsgObject CANMessage;
    } MAIL;

    void CANMessageTX(uint32_t ui32Base, MAIL *mail, tMsgObjType eMsgType = MSG_OBJ_TYPE_TX) {
        CANMessageSet(ui32Base, mail->box_number, &mail->CANMessage, eMsgType);
    }

    void CANMessageRX(uint32_t ui32Base, MAIL *mail, tMsgObjType eMsgType = MSG_OBJ_TYPE_RX) {
        CANMessageGet(ui32Base, mail->box_number, &mail->CANMessage, eMsgType);
    }

    void CANMsgSetTX(uint32_t ui32Base, MAIL *mail, uint32_t ui32Flags = MSG_OBJ_NO_FLAGS) {
        mail->CANMessage.ui32MsgID = mail->ID; // CAN message ID
        mail->CANMessage.ui32MsgIDMask = 0; // no mask needed for TX
        mail->CANMessage.ui32Flags = ui32Flags; // enable interrupt on TX
        mail->CANMessage.ui32MsgLen = mail->n; // size of message is n
        mail->CANMessage.pucMsgData = mail->pMsgData; // ptr to message content
    }

    void CANMsgSetRX(uint32_t ui32Base, MAIL *mail, uint32_t ui32Flags = MSG_OBJ_NO_FLAGS) {
        mail->CANMessage.ui32MsgID = mail->ID; // CAN message ID
        mail->CANMessage.ui32MsgIDMask = 0; // no mask needed for RX
        mail->CANMessage.ui32Flags = ui32Flags; // enable interrupt on RX
        mail->CANMessage.ui32MsgLen = mail->n; // size of message is n
        mail->CANMessage.pucMsgData = mail->pMsgData; // ptr to message content
        CANMessageSet(ui32Base, mail->box_number, &mail->CANMessage, MSG_OBJ_TYPE_RX);
    }

    void initialize(uint32_t BASE, Uint16 pinTX, Uint16 pinRX, Uint16 muxPosTX, Uint16 muxPosRX) {
        GPIO_SetupPinMux(pinTX, GPIO_MUX_CPU1, muxPosTX);
        GPIO_SetupPinMux(pinRX, GPIO_MUX_CPU1, muxPosRX);
        GPIO_SetupPinOptions(pinTX, GPIO_INPUT, GPIO_ASYNC);
        GPIO_SetupPinOptions(pinRX, GPIO_OUTPUT, GPIO_PUSHPULL);
        // Initialize the CAN controller
        CANInit(BASE);
    }

    void enable(uint32_t BASE, char data) {
        CANEnable(BASE);
        if (data) {
            CANGlobalIntEnable(BASE, data);
        }
    }

    void setup(uint32_t BASE, uint32_t bitRate) {
        // Setup CAN to be clocked off the PLL output clock (500kHz CAN-Clock)
        CANClkSourceSelect(BASE, 0);
        // Set up the bit rate for the CAN bus.
        CANBitRateSet(BASE, 200000000, bitRate);
        // Enable interrupts on the CAN peripheral
        CANIntEnable(BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);
    }
}
#endif //CAN_DEF_H
