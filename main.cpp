//
// Included Files
//
#include "F28x_Project.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_can.h"
#include "driverlib/can.h"

#include "CAN_A.h"
#include "CAN_B.h"

//
// Globals
//
volatile unsigned long g_ulTxMsgCount = 0;  // A counter that keeps track of the
                                            // number of times the TX interrupt
                                            // has occurred, which should match
                                            // the number of TX messages that
                                            // were sent.

volatile unsigned long g_ulRxMsgCount = 0;
unsigned long u32CanAErrorStatus;
volatile unsigned long g_bErrFlag = 0;      // A flag to indicate that some
                                            // transmission error occurred.
#include <stdio.h>
//
// Main
//
int main() {
    //
    // Step 1. Initialize System Control:
    // PLL, WatchDog, enable Peripheral Clocks
    // This example function is found in the F2837xD_SysCtrl.c file.
    //
    InitSysCtrl();
    //
    // Step 2. Initialize GPIO:
    // This example function is found in the F2837xD_Gpio.c file and
    // illustrates how to set the GPIO to its default state.
    //
    InitGpio();

    CAN_A::initialize();
    CAN_B::initialize();

    CAN_A::setup();
    CAN_B::setup();

    CAN_A::setupMails();
    CAN_B::setupMails();
    //
    // Step 3. Clear all interrupts and initialize PIE vector table:
    // Disable CPU interrupts
    //
    DINT;
    //
    // Initialize the PIE control registers to their default state.
    // The default state is all PIE interrupts disabled and flags
    // are cleared.
    // This function is found in the F2837xD_PieCtrl.c file.
    //
    InitPieCtrl();
    //
    // Disable CPU interrupts and clear all CPU interrupt flags:
    //
    IER = 0x0000;
    IFR = 0x0000;
    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    // This will populate the entire table, even if the interrupt
    // is not used in this example.  This is useful for debug purposes.
    // The shell ISR routines are found in F2837xD_DefaultIsr.c.
    // This function is found in F2837xD_PieVect.c.
    //
    InitPieVectTable();
    //
    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
    // Register interrupt handler in RAM vector table
    //
    EALLOW;
    PieVectTable.CANA0_INT = CAN_A::execute;
    PieVectTable.CANB0_INT = CAN_B::execute;
    EDIS;
    //
    // Enable the CAN interrupt on the processor (PIE).
    //
    PieCtrlRegs.PIEIER9.bit.INTx5 = 1; // CANA0
    PieCtrlRegs.PIEIER9.bit.INTx7 = 1; // CANB0
    IER |= M_INT9; // M_INT9
    EINT;
    // Enable Global interrupt INTM
    ERTM;
    // Enable Global realtime interrupt DBGM

    //
    // Enable the CAN for operation and CAN Global Interrupt line0.
    //
    CAN_A::enable(CAN_GLB_INT_CANINT0);
    CAN_B::enable(CAN_GLB_INT_CANINT0);

#ifdef CLION
    {
#else
    for (;;) {
#endif

    }
}

//
// End of file
//
